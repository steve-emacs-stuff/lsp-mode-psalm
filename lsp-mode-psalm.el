;; Warning: I have no idea what I'm doing here, almost all of this
;; code is just guesses.


(defgroup lsp-psalm nil
  "LSP support for the PHP programming language, using
psalm-language-server."
  :group 'lsp-mode
  :link '(url-link "https://psalm.dev/")
  :package-version '(lsp-mode . "7.0"))

(defcustom lsp-psalm-path
  "psalm-language-server"
  "Path to the psalm language server.  Try `composer global require vimeo/psalm`"
  :group 'lsp-psalm
  :type 'file)

;; (defun lsp-psalm-server-start-fun (port)
;;   "Define psalm-nlanguage-server start function, it requires a PORT."
;;   `(,lsp-psalm-path
;;     ;; "--root" ,default-directory ??
;;     "--tcp" ,(format "127.0.0.1:%d" port)))

(add-to-list 'lsp-language-id-configuration '(php-mode . "php"))
(lsp-register-client
 (make-lsp-client
  :new-connection (lsp-stdio-connection (list lsp-psalm-path))
  ;; :new-connection (lsp-stdio-connection `(,lsp-psalm-path))
  :activation-fn (lsp-activate-on "php")
  :initialized-fn (lambda (workspace)
                    (with-lsp-workspace workspace
                      (lsp--set-configuration
                       (lsp-configuration-section "psalm"))))
  :add-on? t
  :server-id 'psalm))

;; (setq lsp-log-io t)                     ;
